package com.example.appviews

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.example.appviews.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        window.statusBarColor = ContextCompat.getColor(this, R.color.gradient_start)

        binding.btnSave.setOnClickListener {
            saveCheck()
        }

        binding.btnClear.setOnLongClickListener {
            clear()
            true
        }

    }

    private fun saveCheck() {
        val mail = binding.emailTextInputLayout.editText!!.text.toString()
        val username = binding.usernameTextInputLayout.editText!!.text.toString()
        val firstName = binding.firstNameTextInputLayout.editText!!.text.toString()
        val lastName = binding.lastNameTextInputLayout.editText!!.text.toString()
        val age = binding.ageTextInputLayout.editText!!.text.toString()

        fun isEmailValid(email: String): Boolean {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        if (mail.isEmpty() || username.isEmpty() || firstName.isEmpty() || lastName.isEmpty() || age.isEmpty()){
            Toast.makeText(this, "One or more fields are empty!", Toast.LENGTH_SHORT).show()
        } else if (!isEmailValid(mail)) {
            Toast.makeText(this, "Email not valid !", Toast.LENGTH_SHORT).show()
        }else if (username.length < 10){
            Toast.makeText(this, "Username can't be less than 10 characters !", Toast.LENGTH_SHORT).show()
        }else if(age.toInt() > 130){
            Toast.makeText(this, "Age not Valid !", Toast.LENGTH_SHORT).show()  
        } else{
            Toast.makeText(this, "Information Saved Successfully !", Toast.LENGTH_SHORT).show()
        }

    }


    private fun clear(){
        binding.emailText.setText("")
        binding.usernameText.setText("")
        binding.firstNameText.setText("")
        binding.lastNameText.setText("")
        binding.ageText.setText("")

    }


}